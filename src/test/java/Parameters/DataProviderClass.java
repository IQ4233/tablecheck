package Parameters;



import org.jblas.DoubleMatrix;
import org.testng.annotations.DataProvider;

public class DataProviderClass {
    @DataProvider(name="table_a")
    public Object[][] getTable() { 
    	DoubleMatrix table_a = new DoubleMatrix(5, 4, 10, 20, 22, 28, 80, 12, 15, 60, 0, 87, 14, 24, 0, 0, 38, 45, 0, 0, 0, 45);
    	DoubleMatrix table_b = new DoubleMatrix(5, 4, 15, 30, 33, 42, 120, 18, 22.5, 90, 0, 130.5, 21, 36, 0, 0, 57, 67.5, 0, 0, 0, 67.5);
        double exchange_rate = 1.5;
        
        return new Object[][] {
        	{table_a, table_b, exchange_rate}
        };        
    }

}
