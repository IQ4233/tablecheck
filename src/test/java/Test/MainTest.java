package Test;


import org.jblas.DoubleMatrix;
import org.testng.annotations.Test;

import Base.InitTest;
import Parameters.DataProviderClass;

public class MainTest extends InitTest {
	
	@Test(priority = 1, groups = {"CheckPriceTable"}, dataProvider = "table_a", dataProviderClass=DataProviderClass.class)
	public void checkTableDimension(DoubleMatrix table_a, DoubleMatrix table_b, double exchange_rate) {		
		System.out.println("TableA Product Variety: " + table_a.getRows());
		System.out.println("TableA Product Num: " + (table_a.getColumns() - 1));
		System.out.println("TableB Product Variety: " + table_b.getRows());
		System.out.println("TableB Product Num: " + (table_b.getColumns() - 1));
		if (table_a.getRows() != table_b.getRows()) {
			System.out.println("Variety Num is not same");
			return;
		}
		if (table_a.getColumns() != table_b.getColumns()) {
			System.out.println("Product Num is not same");
			return;
		}
		System.out.println(table_a.mul(exchange_rate).sub(table_b));
		
	}
}
